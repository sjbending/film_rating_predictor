import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def acquireCSV(file_location, columns):
    df = pd.read_csv(file_location, usecols=columns)
    print('\nLoaded file ' + file_location + ', sample:\n')
    print(df.head())
    print('\n')
    return df

def regulariseData(film_info):
    film_info_norm = (film_info - film_info.mean())/film_info.std();

    return film_info_norm

def costFunction(X, ratings, theta, Lambda):
    m = ratings.shape[0]
    
    predictions = np.dot(X, theta.T)
    err = np.subtract(predictions, ratings)

    J = (1/(2*m)) * np.sum(err**2) + (Lambda/2) * np.sum(np.sum(X**2))
    
    grad  = (1/m) * X.T.dot(err) + Lambda * theta
    
    return J, grad

def gradientDescent(X, y, theta, Lambda, num_iter, alpha):
    J_history = []
    for i in range(num_iter):
        cost, grad = costFunction(X, y, theta, Lambda)
        theta  = theta - np.dot(alpha,grad)
        J_history.append(cost)

    return theta, J_history

my_ratings = acquireCSV('ratings_trunc.csv', [0, 2])
features   = acquireCSV('film_info.csv', list(range(1, 62)))

# make sure features are regularised between -1 and 1
features = regulariseData(features)

# randomly initialise theta
num_movies   = my_ratings.shape[0]
num_features = features.shape[1]

# +1 for theta_0
theta_init = np.random.random(1 + num_features)

# options
Lambda   = 0
num_iter = 100
alpha    = 0.1

my_ratings = my_ratings.values[:,1]

# add column of 1s to start of features (for theta_0)
features = np.hstack((np.ones((num_movies,1)), features.values))

theta, J_hist = gradientDescent(features, my_ratings, theta_init, Lambda, num_iter, alpha)

plt.figure()
plt.plot(J_hist)
plt.xlabel("Iteration")
plt.ylabel("$J(\Theta)$")
plt.title("Gradient descent cost function")

plt.show()

theta_df = pd.DataFrame([theta])
theta_df.to_csv('learned_params.csv')
