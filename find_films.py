import config
import requests

import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import os.path

from urllib.parse import quote

api_key = config.tmdb_api_key

# OPTIONS
plot_features = False
plot_variable = 'Year'
reacquire_features = True

#--------------------------------------------------------------------------#

# Acquire the film ratings from the letterboxd csv
def acquire_ratings(ratings_location):
    ratings = pd.read_csv(ratings_location, usecols=[1, 2, 4])
    ratings.columns = ['Film Title', 'Year', 'Rating']
    print('\nLoaded film ratings from file, sample:\n')
    print(ratings.head())
    print('\n')
    return ratings

#--------------------------------------------------------------------------#

# Acquire film information from TDMB
def acquire_film_info(ratings):
    # appending to regular array for efficiency
    film_data = []
    # loop through films rated to acquire features
    # (looping through itertuples is faster than regular loops)
    for film in ratings.itertuples():
        # search for the films and grab the first result
        # quote url encodes the film title
        film_info = requests.get('https://api.themoviedb.org/3/search/movie?api_key=' + api_key + '&query=' + quote(film[1]) + '&page=1&year=' + str(film[2]))
        film_info = film_info.json()

        # skip film if we can't find it in the database
        # and remove it from the ratings list
        if not film_info['results']:
            print(film[1] + ' not found in database, skipping.\n')
            ratings.drop(film.Index, inplace = True)
            continue

        popularity   = film_info['results'][0]['popularity']
        vote_average = film_info['results'][0]['vote_average']
        year         = film_info['results'][0]['release_date'][0:4]

        # use film id to acquire additional information
        film_id = film_info['results'][0]['id']
        film_info = requests.get('https://api.themoviedb.org/3/movie/' + str(film_id) + '?api_key=' + api_key)
        film_info = film_info.json()

        # acquire genres and shove them into a single string separated by commas
        genre_str = ''
        for genre in film_info['genres']:
            if genre != film_info['genres'][0]:
                genre_str += ', '
            genre_str += genre['name']

        # acquire film language
        language = film_info['original_language']

        film_data.append([film[1], popularity, vote_average, int(year), genre_str, language])
    # now convert to dataframe
    columns = ['Film Title', 'Popularity', 'Vote Average', 'Year', 'Genres', 'Language']
    df = pd.DataFrame(film_data, columns=columns);

    # get list of unique genres
    genre_list = requests.get('https://api.themoviedb.org/3/genre/movie/list?api_key=' + api_key + '&language=en-US').json()    
    unique_genres = []
    for genre in genre_list['genres']:
        unique_genres.append(genre['name'])

    # get list of unique languages
    lang_list = list(np.unique(df['Language']))

    # generate one hot features/dummies
    df = df.reindex(df.columns.tolist() + unique_genres + lang_list, axis=1, fill_value=0)

    # go through genres in dataframe and update dummies appropriately
    for index, row in df.iterrows():
        for val in row[['Genres']].str.split(', '):
            df.loc[index, val] = 1
        for val in row[['Language']]:
            df.loc[index, val] = 1
               
    # remove string and empty columns after we're done
    df.drop('', axis=1, inplace = True)
    df.drop('Genres', axis=1, inplace = True)
    df.drop('Language', axis=1, inplace = True)

    print('Acquired film info from database, sample:\n')
    print(df.head())
    print('\n')
    return df, ratings
            
#------------------------------------------------------------------------#

# Main function
def main():
    my_ratings = acquire_ratings('ratings.csv')

    # acquire film information (i.e. our features) if we don't have it or wish
    # to generate new feature information
    if os.path.isfile('film_info.csv') == False or reacquire_features == True:
        film_info, my_ratings = acquire_film_info(my_ratings)

        print(str(len(film_info)) + ' films in film info, ' + str(len(my_ratings)) + ' films in ratings; these should be the same.')
        
        film_info.to_csv('film_info.csv', index = False)
        my_ratings.to_csv('ratings_trunc.csv', index = False)
    else:
        film_info = pd.read_csv('film_info.csv')
        print('Loaded film info from file, sample:\n')
        print(film_info.head())
        print('\n')

        if plot_features == True:
            matplotlib.style.use('ggplot')
            fig, ax = plt.subplots()
            fig.set_size_inches(14, 9)
            
            x, y = [], []
            for film in my_ratings.itertuples():
                if not film_info[film_info['Film Title'] == film[1]].empty:
                    x.append(film_info.loc[film_info['Film Title'] == film[1]][plot_variable].to_numpy()[0]) # note: having to do some gymnastics here to get the dataframe output to not be an array
                    y.append(film[3])
                    
            xbins = 50 #int(max(x) - min(x) / 20);
            ybins = 10 #int(max(y) - min(y) / 5);
                    
            plt.hist2d(x, y, bins = [xbins, ybins])
            ax.set(xlabel=plot_variable, ylabel='Rating', xlim = [1900, 2020])

            if(plot_variable == 'Vote Average'):
                degree = 2
            else:
                degree = 1

            poly1d_fn = np.poly1d(np.polyfit(x, y, degree))
            plt.plot(x, poly1d_fn(x), '-r')
                                        
            plt.colorbar()
                    
            plt.tight_layout()
            plt.show()

if __name__ == "__main__":
    main()
