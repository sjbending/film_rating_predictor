import config
import requests

import numpy as np
import pandas as pd

from urllib.parse import quote

api_key = config.tmdb_api_key

def acquireCSV(file_location, columns):
    df = pd.read_csv(file_location, usecols=columns)
    return df

def regulariseData(cur_features):
    all_features = acquireCSV('film_info.csv', list(range(1, 62)))

    features_norm = (cur_features - all_features.mean())/all_features.std();

    return features_norm

def findFilmFeatures(film_name, year):
    film_info = requests.get('https://api.themoviedb.org/3/search/movie?api_key=' + api_key + '&query=' + quote(film_name) + '&page=1&year=' + str(year))
    film_info = film_info.json()

    popularity = film_info['results'][0]['popularity']
    vote_average = film_info['results'][0]['vote_average']
    year = film_info['results'][0]['release_date'][0:4]

    # use film id to acquire additional information
    film_id = film_info['results'][0]['id']
    film_info = requests.get('https://api.themoviedb.org/3/movie/' + str(film_id) + '?api_key=' + api_key).json()
    
    # acquire genres and shove them into a single string
    genre_str = ''
    for genre in film_info['genres']:
        if genre != film_info['genres'][0]:
            genre_str += ', '
        genre_str += genre['name']

    # acquire film language
    language = film_info['original_language']
    
    columns = ['Popularity', 'Vote Average', 'Year', 'Genres', 'Language']
    features = pd.DataFrame([[popularity, vote_average, int(year), genre_str]], columns=columns)

    # get list of unique genres for one hot dummies
    

def predictRating(theta, X):
    prediction = np.dot(X, theta.T)
    return prediction

theta = acquireCSV('learned_params.csv', list(range(0, 61))

film_name = input('\nEnter film title: ')
film_year = input('Enter year of film release (in case of duplicates): ')

features = findFilmFeatures(film_name, film_year)

print('\n' + film_name + ' has the following features:\n')
print(features)

features = regulariseData(features)

# insert one for theta0
features = np.insert(features.values, 0, 1)

prediction = predictRating(theta.values, features)

print('\nPredicted rating (out of 5) is:')
print(prediction)
print('\n')
